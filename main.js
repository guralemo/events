document.addEventListener("DOMContentLoaded", function () {
  const joinProgram = document.createElement("section");
  joinProgram.className = "app-section app-section--join-our-program";
  const title = document.createElement("h2");
  title.className = "app-title";
  title.textContent = "Join Our Program";
  const subtitle = document.createElement("h3");
  subtitle.className = "app-subtitle";
  subtitle.textContent =
    "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";

  const form = document.createElement("form");
  form.action = "email";
  form.id = "subscribeForm";
  const inputEmail = document.createElement("input");
  inputEmail.type = "email";
  inputEmail.className = "input-email";
  inputEmail.placeholder = "EMAIL";
  inputEmail.id = "email";
  inputEmail.name = "email";
  inputEmail.value = "email";

  const submitButton = document.createElement("button");
  submitButton.type = "submit";
  submitButton.className =
    "app-section__button subscribe-button app-section__button--read-more";
  submitButton.textContent = "Subscribe";

  form.appendChild(inputEmail);
  form.appendChild(submitButton);
  joinProgram.appendChild(title);
  joinProgram.appendChild(subtitle);
  joinProgram.appendChild(form);

  const footer = document.querySelector("footer");
  footer.before(joinProgram);
  const subscribeForm = document.getElementById("subscribeForm");
  subscribeForm.addEventListener("submit", function (event) {
    event.preventDefault();
    const enterEmail = document.getElementById("email");
    console.log("Entered email is:", enterEmail.value);
  });
});

//The same code but here whit using innerHTML

/*
document.addEventListener("DOMContentLoaded", function () {
  const joinProgram = document.createElement("section");
  joinProgram.classList.add(
    "app-section app-section app-section--join-our-program"
  );

  joinProgram.innerHTML = `
    <section class="app-section app-section--join-our-program">
      <h2 class="app-title">Join Our Program</h2>
      <h3 class="app-subtitle">
        Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      </h3>
      <form action="email" id="subscribeForm">
        <input
          type="email"
          class="input-email"
          placeholder="EMAIL"
          id="email"
          name="email"
          value="email"
        />
        <button type="submit" class="app-section__button subscribe-button app-section__button--read-more">
          Subscribe
        </button>
      </form>
    </section>
  `;
  const footer = document.querySelector("footer");
  footer.before(joinProgram);
  const subscribeForm = document.getElementById("subscribeForm");
  subscribeForm.addEventListener("submit", function (event) {
    event.preventDefault();

    const enterEmail = document.getElementById("email");
    console.log("Entered email is:", enterEmail.value);
  });
});
document.addEventListener("DOMContentLoaded", joinProgramSection);
*/

// document.addEventListener("load", joinProgramSection);
